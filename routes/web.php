<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SkillController;
use App\Http\Controllers\ExperienceController;
use App\Http\Controllers\frontController;
use App\Http\Controllers\MailController;
use App\Http\Controllers\ProjectController;
use PhpParser\Node\Expr\FuncCall;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});


Route::get('/dashboard', function () {
    return view('admin.index');
})->name('dashboard');


Route::get('/', [frontController::class, 'index']);
Route::get('/mail', [MailController::class, 'index']);

Route::resource('skills', SkillController::class);
Route::resource('experiences', ExperienceController::class);
Route::resource('projects', ProjectController::class);
