<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
    public function index(){
        // Normal mail send

        // Mail::raw('hello world', function($message){
        //     $message->from('from@example.com');
        //     $message->to('to@example.com');
        //     $message->subject('Congrats, your mail has been send.');
        // });

        // send a html page  mail

        Mail::send(['html'=>'mail'], ['name'=>'Rohinee'], function($message){
                $message->from('from@example.com');
                $message->to('to@example.com');
                $message->subject('Congrats, your mail has been send.');

        });
        return view('mail');
    }
}
