<?php

namespace App\Http\Controllers;
use App\Http\Requests\StoreExperience;
use App\Models\Experience;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;



class ExperienceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $experiences = Experience::get();
        return view('admin.experiences.index', compact('experiences'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.experiences.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreExperience $request)
    {
        $experiences = new Experience;
        $experiences->title = $request->title;
        $experiences->date = $request->date;
        $experiences->description = $request->description;
        $experiences->save();
        return redirect('/experiences')->with(['success' => 'Experience has been created.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $experiences = Experience::find($id);
        return view('admin.experiences.show', compact('experiences'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $experiences = Experience::find($id);
        return view('admin.experiences.edit', compact('experiences'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $experiences = Experience::find($id);
        $experiences->title = $request->title;
        $experiences->date = $request->date;
        $experiences->description = $request->description;
        $experiences->save();
        return redirect('/experiences');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Experience::find($id)->delete();
        return redirect('/experiences')->with(['success' => 'Experience has been deleted.']);
    }
}
