<?php

namespace App\Http\Controllers;

use App\Models\Experience;
use App\Models\Project;
use App\Models\Skill;
use Illuminate\Http\Request;

class frontController extends Controller
{
    public function index(){

        $skills = Skill::get();
        $experiences = Experience::get();
        $projects = Project::get();
        return view('index', compact('skills', 'experiences', 'projects'));
    }
}
