<?php

namespace App\Http\Controllers;
use Illuminate\Http\Requests;
use App\Http\Requests\Skills\StoreSkill;
use App\Models\Skill;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class SkillController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $skills = Skill::get();
        return view('admin.skills.index', compact('skills'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function create()
    {
        return view('admin.skills.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(StoreSkill $request)
    {
        $skills = new Skill;
        $skills->name = $request->name;
        $skills->status = $request->status;
        $skills->save();
        return redirect('/skills')->with(['success' => 'Skill has been created.']);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function show($id)
    {
        $skills = Skill::find($id);
        return view('admin.skills.show', compact('skills'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function edit($id)
    {
        $skills = Skill::find($id);
        return view('admin.skills.edit', compact('skills'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request, $id)
    {
        $skills = Skill::find($id);
        $skills->name = $request->name;
        $skills->status = $request->status;
        $skills->save();
        return redirect('/skills');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy($id)
    {
        $skills = Skill::find($id)->delete();
        return redirect('/skills')->with(['success' => 'Skill has been deleted.']);
    }
}
