<?php

namespace App\Observers;

use App\Models\Project;
use Illuminate\Support\Facades\File;

class ProjectObserver
{
    public function creating(Project $projects){

        $file_name = time().'.'.request()->file->extension();
        $projects->photo = $file_name;
    }

    public function created(Project $projects){

        if (request()->file) {
            $path = public_path('assets/uploads/project-uploads/projects');

            if (!file_exists($path)) {
                File::makeDirectory($path, $mode = 0777, true, true);
            }
            request()->file->move($path, $projects->photo);
        }
    }

    public function updating(Project $projects){

        if (request()->file) {

            $path = public_path('assets/uploads/project-uploads/projects');
            $this->deleteFile($path.$projects->photo);

            $file_name = time().'.'.request()->file->extension();
            $projects->photo = $file_name;
        }
    }

    public function updated(Project $projects){

        $path = public_path('assets/uploads/project-uploads/projects');

        if (!file_exists($path)) {

            File::makeDirectory($path, $mode = 0777, true, true);
        }
        request()->file->move($path, $projects->photo);
    }

    public function deleted(Project $projects)
    {
        $path = public_path('assets/uploads/project-uploads/projects');

        // Old file delete code
        $this->deleteFile($path.$projects->photo);
    }

    public function deleteFile($path)
    {
        File::delete($path);
    }
}




