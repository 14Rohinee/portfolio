<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Observers\SkillObserver;
use Illuminate\Notifications\Notifiable;

class Skill extends Model
{
    use HasFactory, Notifiable;
    protected static function boot() {
        parent::boot();
        static::observe(SkillObserver::class);
    }
    protected $fillable = [
        'name',
        'photo',
        'status',
    ];
}
