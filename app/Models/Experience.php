<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Experience extends Model
{
    use HasFactory;
    protected $fillable = [
        'title',
        'date',
        'description',
    ];
      // Mutator
      public function setDateAttribute($value)
      {
          $this->attributes['date'] = date('Y-m-d', strtotime($value));
      }

      // Accessor
      public function getDateAttribute($value)
      {
          return date('d M, Y', strtotime($value));
      }
}
