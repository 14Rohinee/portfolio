<?php

namespace App\Models;

use App\Observers\ProjectObserver;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Project extends Model
{
    use HasFactory, Notifiable;

    // protected static function boot() {
    //     parent::boot();
    //     static::observe(ProjectObserver::class);
    // }
    protected $fillable = [
        'name',
        'created_on',
        'description',
        'photo',
    ];
      // Mutator
      public function setCreatedOnAttribute($value)
      {
          $this->attributes['created_on'] = date('Y-m-d', strtotime($value));
      }

      // Accessor
      public function getCreatedOnAttribute($value)
      {
          return date('d M, Y', strtotime($value));
      }
}
