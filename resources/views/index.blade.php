<!doctype html>
<html lang="en">

<!-- Mirrored from cvresumetemplate.com/maha-personal-cv-resume-html-template/home-two.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 07 Aug 2021 15:29:05 GMT -->

<head>
    <title>Rohinee's Portfolio</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- FAV AND ICONS   -->
    <link rel="shortcut icon" href="{{ asset('assets/images/title.png') }}">

    <!-- Google Font-->
    <link href="http://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('assets/icons/font-awesome-4.7.0/css/font-awesome.min.css') }}">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('assets/plugins/css/bootstrap.min.css') }}">
    <!-- Animate CSS-->
    <link rel="stylesheet" href="{{ asset('assets/plugins/css/animate.css') }}">
    <!-- Owl Carousel CSS-->
    <link rel="stylesheet" href="{{ asset('assets/plugins/css/owl.css') }}">
    <!-- Fancybox-->
    <link rel="stylesheet" href="{{ asset('assets/plugins/css/jquery.fancybox.min.css') }}">
    <!-- Custom CSS-->

    <link rel="stylesheet" href="{{ asset('assets/css/new.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/styles.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/responsive.css') }}">

</head>

<body class="dark-vertion black-bg">

    <!-- Start Loader -->
    <div class="section-loader">
        <div class="loader">
            <div></div>
            <div></div>
        </div>
    </div>
    <!-- End Loader -->

    <!--
===================
NAVIGATION
===================
-->
    <header class="black-bg mh-header mh-fixed-nav nav-scroll mh-xs-mobile-nav" id="mh-header">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <nav class="navbar navbar-expand-lg mh-nav nav-btn">
                    <a class="navbar-brand" href="#">
                        <img src="assets/images/logo.png" alt="maha" class="img-fluid" height="50" width="50">
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-0 ml-auto">
                            <li class="nav-item active" id="active">
                                <a class="nav-link" href="#mh-home">Home</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#mh-about">About</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#mh-skills">Skills</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#mh-experience">Experiences</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#mh-blog">Projects</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#mh-contact">Contact</a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </header>

    <!--
===================
Home
===================
-->
    <section class="mh-home image-bg" id="mh-home">
        <div>
            <div class="container">
                <div class="row section-separator xs-column-reverse vertical-middle-content home-padding">

                    <div class="col-sm-6 col-md-8">
                        <div class="mh-header-info">
                            <div class="mh-promo wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.1s">
                                <span>Hello I'm</span>
                            </div>

                            <h1 class="wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.2s">Rohinee Tandekar
                            </h1>
                            <h4 class="wow fadeInUp designation" data-wow-duration="0.8s" data-wow-delay="0.3s">Full
                                stack web developer</h4>
                            <p class="wow fadeInUp intro" data-wow-duration="0.8s" data-wow-delay="0.3s">I'm a creative
                                web developer and designer. <br>
                                specialized in designing and developing responsive websites. <br>
                                Currently I'm fresher on webdev,
                                and I'm looking for the oppertunity to show my skills.
                            </p>
                            <ul>
                                <li class="wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.4s"><i
                                        class="fa fa-envelope"></i><a href="mailto:">rohineetandekar14@gmail.com</a>
                                </li>

                                <li class="wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.6s"><i
                                        class="fa fa-map-marker"></i>
                                    <address>Mahasamund Chhattisagarh(India)</address>
                                </li>
                            </ul>

                            <ul class="social-icon wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.7s">
                                <li><a href="https://www.linkedin.com/in/rohinee-tandekar-97bb9a1b2/" target="_blank"><i
                                            class="fa fa-linkedin"></i></a></li>
                                <li><a href="https://gitlab.com/14Rohinee" target="_blank"><i
                                            class="fa fa-gitlab"></i></a></li>
                                <li><a href="https://github.com/14Rohinee" target="_blank"><i
                                            class="fa fa-github"></i></a></li>
                                <li><a href="https://twitter.com/call_me_ruhi" target="_blank"><i
                                            class="fa fa-twitter"></i></a></li>
                                <li><a href="https://www.facebook.com/rohineetandekar/" target="_blank"><i
                                            class="fa fa-facebook"></i></a></li>
                                <li><a href="https://www.instagram.com/rohineetandekar/" target="_blank"><i
                                            class="fa fa-instagram"></i></a></li>

                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-8">

                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--
===================
ABOUT
===================
-->
    <section class="mh-about" id="mh-about">
        <div class="container">
            <div class="row section-separator">
                <div class="col-sm-12 col-md-6 img-sec">
                    <div class="mh-about-img shadow-2 wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.4s">
                        <img src="assets/images/me.png" alt="about-image" class="img-fluid">
                    </div>
                </div>
                <div class="col-sm-12 col-md-6">
                    <div class="mh-about-inner">
                        <h2 class="wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.1s" id="about">About Me
                        </h2>
                        <p class="wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.2s">Hello! My name is
                            Rohinee, I enojoy designing and creating simple and beautiful things and I love what I
                            do.Since
                            my college projects time I love to design and create websites. Obviously I'm a fresher right
                            now
                            but I'm curiously waiting for the opportunity to show my skills.</p>
                        <a href="https://drive.google.com/file/d/1dRiR2jCFBsDcnqDvG5I5TOYlxHwfbGtU/view?usp=sharing"
                            target="_blank" class="btn btn-fill wow fadeInUp" data-wow-duration="0.8s"
                            data-wow-delay="0.4s">Download Resume<i class="fa fa-download"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <!--
===================
SKILLS
===================
-->
    <section class="mh-skills" id="mh-skills">
        <div class="home-v-img">
            <div class="container">
                <div class="row section-separator">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <div class="mh-skills-inner">
                            <div class="mh-professional-skill wow fadeInUp" data-wow-duration="0.8s"
                                data-wow-delay="0.3s">
                                <h3 id="skills">Skills</h3>
                                <div class="container skill-container">
                                    <div class="row skill-row">
                                        @foreach ($skills as $skill)
                                            <div class="col-md-3 col-sm-3 col-3 wow fadeInUp mt-5"
                                                data-wow-duration="0.8s" data-wow-delay="0.3s">
                                                <img src="{{ asset('assets/uploads/skill-uploads/skills/' . $skill->photo) }}"
                                                    alt="laravel"><br>
                                                <span class="skill-name">{{ $skill->name }}</span>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--
===================
EXPERIENCES
===================
-->
    <section class="mh-experince image-bg featured-img-one" id="mh-experience">
        <div class="img-color-overlay">
            <div class="container">
                <div class="row section-separator">
                    <div class="col-sm-12 col-md-6">
                        <div class="mh-education">
                            <h3 class="wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.2s" id="education">
                                Education</h3>
                            <div class="mh-education-deatils">
                                <!-- Education Institutes-->
                                <div class="mh-education-item dark-bg wow fadeInUp" data-wow-duration="0.8s"
                                    data-wow-delay="0.3s">
                                    <h4>Bachlor's of Technology From <a href="#">CSVTU, Bhilai</a></h4>
                                    <div class="mh-eduyear">2016-2020</div>
                                    <p>I have been completed my B.Tech degree from MM college of Technology Raipur(C.G.)
                                        with 69.60% aggregate.</p>
                                </div>
                                <!-- Education Institutes-->
                                <div class="mh-education-item dark-bg wow fadeInUp" data-wow-duration="0.8s"
                                    data-wow-delay="0.5s">
                                    <h4>Higher Secondary School From <a href="#">CGBSE, Raipur</a></h4>
                                    <div class="mh-eduyear">2015-2016</div>
                                    <p>I have been completed my Secondary School from Public higher Secondary School
                                        Tumgaon(C.G.) with 71.2%. </p>
                                </div>
                                <!-- Education Institutes-->
                                <div class="mh-education-item dark-bg wow fadeInUp" data-wow-duration="0.8s"
                                    data-wow-delay="0.6s">
                                    <h4>Higher Secondary School From <a href="#">CGBSE, Raipur</a></h4>
                                    <div class="mh-eduyear">2013-2014</div>
                                    <p>I have been completed my Primary School from Public higher Secondary School
                                        Tumgaon(C.G.) with 67%.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <div class="mh-work">
                            <h3 id="work">Work Experience</h3>
                            <div class="mh-experience-deatils">
                                <!-- Education Institutes-->
                                @foreach ($experiences as $experience)
                                    <div class="mh-work-item dark-bg wow fadeInUp" data-wow-duration="0.8s"
                                        data-wow-delay="0.4s">
                                        <h4>{{ $experience->title }}</h4>
                                        <br>
                                        {{-- <div class="mh-eduyear">{{ $experience->date }}</div> --}}
                                        <p>{{ $experience->description }}</p>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--
===================
Projects
===================
-->
    <section class="mh-blog image-bg featured-img-two" id="mh-blog">
        <div class="img-color-overlay">
            <div class="container">
                <div class="row section-separator">
                    <div class="col-sm-12 section-title wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.2s">
                        <h3 id="projects">Projects</h3>
                    </div>
                    @foreach ($projects as $project)

                        <div class="col-sm-12 col-md-4">
                            <div class="mh-blog-item dark-bg wow fadeInUp" data-wow-duration="0.8s"
                                data-wow-delay="0.3s">
                                <img img
                                    src="{{ asset('assets/uploads/project-uploads/projects/' . $project->photo) }}"
                                    alt="" class="img-fluid">
                                <div class="blog-inner">
                                    <h2><a href="blog-single.html">{{ $project->name }}</a></h2>
                                    <div class="mh-blog-post-info">
                                        <ul>
                                            <li><strong>Created On</strong><a href="#">{{ $project->created_on }}</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <p>{{ $project->description }}</p>
                                    {{-- <a href="#">Read More</a> --}}
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>


    <!--
===================
FOOTER 3
===================
-->
    <footer class="mh-footer mh-footer-3" id="mh-contact">
        <div class="container-fluid">
            <div class="row section-separator">
                <div class="col-sm-12 section-title wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.2s">
                    <h3 id="contact">Contact Me</h3>
                </div>
                <div class="map-image image-bg col-sm-12">
                    <div class="container mt-30">
                        <div class="row">
                            <div class="col-sm-12 col-md-6 mh-footer-address">
                                <div class="col-sm-12 xs-no-padding">
                                    <div class="mh-address-footer-item dark-bg shadow-1 media wow fadeInUp"
                                        data-wow-duration="0.8s" data-wow-delay="0.2s">
                                        <div class="each-icon">
                                            <i class="fa fa-location-arrow"></i>
                                        </div>
                                        <div class="each-info media-body">
                                            <h4>Address</h4>
                                            <address>
                                                01. Ambedakar Chauk Nagar Panchayat Tumgaon(C.G.)
                                            </address>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 xs-no-padding">
                                    <div class="mh-address-footer-item media dark-bg shadow-1 wow fadeInUp"
                                        data-wow-duration="0.8s" data-wow-delay="0.4s">
                                        <div class="each-icon">
                                            <i class="fa fa-envelope-o"></i>
                                        </div>
                                        <div class="each-info media-body">
                                            <h4>Email</h4>
                                            <a href="mailto:yourmail@email.com">rohineetandekar14@gmail.com</a><br>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 xs-no-padding">
                                    <div class="mh-address-footer-item media dark-bg shadow-1 wow fadeInUp"
                                        data-wow-duration="0.8s" data-wow-delay="0.6s">
                                        <div class="each-icon">
                                            <i class="fa fa-phone"></i>
                                        </div>
                                        <div class="each-info media-body">
                                            <h4>Linkedin</h4>
                                            <a href="https://www.linkedin.com/in/rohinee-tandekar-97bb9a1b2/"
                                                target="_blank ">www.linkedin.com/in/rohinee-tandekar-97bb9a1b2/</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 wow fadeInUp" data-wow-duration="0.8s"
                                data-wow-delay="0.2s">
                                <form id="contactForm" class="single-form quate-form wow fadeInUp"
                                    data-toggle="validator">
                                    <div id="msgSubmit" class="h3 text-center hidden"></div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <input name="name" class="contact-name form-control" id="name" type="text"
                                                placeholder="First Name" required>
                                        </div>

                                        <div class="col-sm-12">
                                            <input name="name" class="contact-email form-control" id="L_name"
                                                type="text" placeholder="Last Name" required>
                                        </div>

                                        <div class="col-sm-12">
                                            <input name="name" class="contact-subject form-control" id="email"
                                                type="email" placeholder="Your Email" required>
                                        </div>

                                        <div class="col-sm-12">
                                            <textarea class="contact-message" id="message" rows="6"
                                                placeholder="Your Message" required></textarea>
                                        </div>

                                        <!-- Subject Button -->
                                        <div class="btn-form col-sm-12">
                                            <button type="submit" class="btn btn-fill btn-block" id="form-submit">Send
                                                Message</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-sm-12 mh-copyright wow fadeInUp" data-wow-duration="0.8s"
                                data-wow-delay="0.2s">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="text-left text-xs-center">
                                            <p id="footer">All right reserved Rohinee Tandekar</p>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <ul class="social-icon">
                                            <li><a href="https://www.linkedin.com/in/rohinee-tandekar-97bb9a1b2/"
                                                    target="_blank"><i class="fa fa-linkedin fa-2x"></i></a></li>
                                            <li><a href="https://gitlab.com/14Rohinee" target="_blank"><i
                                                        class="fa fa-gitlab fa-2x"></i></a></li>
                                            <li><a href="https://github.com/14Rohinee" target="_blank"><i
                                                        class="fa fa-github fa-2x"></i></a></li>
                                            <li><a href="https://twitter.com/call_me_ruhi" target="_blank"><i
                                                        class="fa fa-twitter fa-2x"></i></a></li>
                                            <li><a href="https://www.facebook.com/rohineetandekar/" target="_blank"><i
                                                        class="fa fa-facebook fa-2x"></i></a></li>
                                            <li><a href="https://www.instagram.com/rohineetandekar/" target="_blank"><i
                                                        class="fa fa-instagram fa-2x"></i></a></li>
                                        </ul>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!--
==============
* JS Files *
==============
-->

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->

    <!-- jQuery -->
    <script src="{{ asset('assets/plugins/js/jquery.min.js') }}"></script>
    <!-- popper -->
    <script src="{{ asset('assets/plugins/js/popper.min.js') }}"></script>
    <!-- bootstrap -->
    <script src="{{ asset('assets/plugins/js/bootstrap.min.js') }}"></script>
    <!-- owl carousel -->
    <script src="{{ asset('assets/plugins/js/owl.carousel.js') }}"></script>
    <!-- validator -->
    <script src="{{ asset('assets/plugins/js/validator.min.js') }}"></script>
    <!-- wow -->
    <script src="{{ asset('assets/plugins/js/wow.min.js') }}"></script>
    <!-- mixin js-->
    <script src="{{ asset('assets/plugins/js/jquery.mixitup.min.js') }}"></script>
    <!-- circle progress-->
    <script src="{{ asset('assets/plugins/js/circle-progress.js') }}"></script>
    <!-- jquery nav -->
    <script src="{{ asset('assets/plugins/js/jquery.nav.js') }}"></script>
    <!-- Fancybox js-->
    <script src="{{ asset('assets/plugins/js/jquery.fancybox.min.js') }}"></script>
    <!-- isotope js-->
    <script src="{{ asset('assets/plugins/js/isotope.pkgd.js') }}"></script>
    <script src="{{ asset('assets/plugins/js/packery-mode.pkgd.js') }}"></script>
    <!-- Custom Scripts-->
    <script src="{{ asset('assets/js/custom-scripts.js') }}"></script>

</body>

</html>
