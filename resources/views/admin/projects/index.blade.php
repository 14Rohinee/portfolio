@extends('layouts.dashboard')

@section('title')
    Projects
@endsection

@section('css')
    <link href="{{ asset('assets/css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
@endsection

@section('breadcrum')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
        <h2>Projects</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ route('dashboard') }}">Home</a>
            </li>
            <li class="active">
                <strong>Projects</strong>
            </li>
        </ol>
        </div>
        <div class="col-lg-2">
            <a href="{{ route('projects.create') }}" class="btn btn-primary mt-3">
                <span class="fa fa-plus"></span>
                Add</a>
        </div>
    </div>
@endsection

@section('content')

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Projects List
                    </h5>
                </div>
                <div class="ibox-content">

                <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover projects-table" >
                    <thead>
                        <tr>
                            <th>Photo</th>
                            <th>Name</th>
                            <th>Created On</th>
                            <th>Description</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($projects as $project)
                            <tr>
                                <td>
                                    {{-- {{ asset('assets/uploads/project-uploads/projects/'. $project->photo ) }} --}}

                                    <img src="{{ asset('assets/uploads/project-uploads/projects/'. $project->photo ) }}" alt="" height="80" width="80">

                                </td>
                                <td>{{ $project->name }}</td>
                                <td>{{ $project->created_on }}</td>
                                <td>{{ $project->description }}</td>

                                <td>
                                    <div class="btn-group">
                                        <a href="{{ route('projects.show', $project->id) }}" class="btn-white btn btn-xs" >View</a>

                                        <a href="{{ route('projects.edit', $project->id) }}" class="btn-white btn btn-xs">Edit</a>

                                        <form action="{{ route('projects.destroy',$project->id) }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn-white btn btn-xs">Delete</button>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
                </div>
            </div>
        </div>
        </div>
    </div>
@endsection

@section('script')
    {{-- Datatable --}}
    <script src=" {{ asset('assets/js/plugins/dataTables/datatables.min.js') }}"></script>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            $('.projects-table').DataTable();

            @if(session('success'))
                toastr.success('{{ session("success") }}', 'Success');
            @endif


            $('.delete-project').click(function () {

                let projectId = $(this).data('project-id');

                // Generate Token method 1
                // let token = $("meta[name='csrf-token']").attr("content");

                // Generate Token method 2
                let token = '{{ csrf_token() }}';

                let url = '{{ route("projects.destroy", ":id") }}';
                url = url.replace(':id', projectId);

                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this data!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                }, function () {

                    $.ajax(
                    {
                        url: url,
                        type: 'POST',
                        data: {
                            "_token": token,
                            "_method": 'DELETE',

                        },
                        success: function (){
                            swal("Deleted!", "project account has been deleted.", "success");
                            setTimeout(() => {
                                location.reload();
                            }, 3000);
                        }
                    });

                });
            });

        });
    </script>
@endsection
