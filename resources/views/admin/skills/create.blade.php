@extends('layouts.dashboard')

@section('title')
    Add Skill
@endsection

@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.min.css" integrity="sha512-EZSUkJWTjzDlspOoPSpUFR0o0Xy7jdzW//6qhUkoZ9c4StFkVsp9fbbd0O06p9ELS3H486m4wmrCELjza4JEog==" crossorigin="anonymous" referrerpolicy="no-referrer" />
@endsection


@section('breadcrum')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Create Skill</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('dashboard') }}">Home</a>
                </li>
                <li>
                    <a href="{{ route('skills.index') }}">Skills</a>
                </li>
                <li class="active mt-3">
                    <strong>Add</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">
            <button class="btn btn-primary mt-3" id="save-data"> <span class="fa fa-save"></span> Save</button>
        </div>
    </div>
@endsection

@section('content')
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <form action="{{ route('skills.store') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="col-lg-9">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h3>Skill Details</h3>
                        </div>
                        <div class="ibox-content">
                            <div class="row">
                                    <div class="col-md-12">

                                        <div class="form-group @error('name') has-error @enderror">
                                            <label for="name">Name <span class="text-danger">*</span></label>
                                            <input type="text" placeholder="Enter name" class="form-control" name="name" id="name" value="{{ old('name') }}">
                                            @error('name')
                                                <span class="text-danger"> {{ $message }} </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group @error('status') has-error @enderror">
                                            <label for="status">Status</label>
                                            <select class="form-control" name="status" id="status">
                                                <option>Select</option>
                                                <option @if(old('status') == 'active') selected @endif value="active">Active</option>
                                                <option  @if(old('status') == 'inactive') selected @endif value="inactive">Inactive</option>
                                                @error('status')
                                                <span class="text-danger"> {{ $message }} </span>
                                            @enderror
                                            </select>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Uploaded Photo</h5>
                        </div>
                        <div class="ibox-content">
                            <input type="file" class="dropify" name="file" />
                            @error('file')
                            <span class="text-danger"> {{ $message }} </span>
                            @enderror
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js" integrity="sha512-8QFTrG0oeOiyWo/VM9Y8kgxdlCryqhIxVeRpWSezdRRAvarxVtwLnGroJgnVW9/XBRduxO/z1GblzPrMQoeuew==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>


    <script>
        $('.dropify').dropify();

        $(document).on('click', '#save-data', function(){
            $('form').submit();
        });


    </script>

@endsection

