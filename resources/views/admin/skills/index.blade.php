@extends('layouts.dashboard')

@section('title')
    Skills
@endsection

@section('css')
    <link href="{{ asset('assets/css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
@endsection

@section('breadcrum')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
        <h2>Skills</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ route('dashboard') }}">Home</a>
            </li>
            <li class="active">
                <strong>Skills</strong>
            </li>
        </ol>
        </div>
        <div class="col-lg-2">
            <a href="{{ route('skills.create') }}" class="btn btn-primary mt-3">
                <span class="fa fa-plus"></span>
                Add</a>
        </div>
    </div>
@endsection

@section('content')

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Skills List
                    </h5>
                </div>
                <div class="ibox-content">

                <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover skills-table" >
                    <thead>
                        <tr>
                            <th>Photo</th>
                            <th>Name</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($skills as $skill)
                            <tr>
                                <td>
                                    <img src="{{ asset('assets/uploads/skill-uploads/skills/'. $skill->photo ) }}" alt="" height="80" width="80">

                                </td>
                                <td>{{ $skill->name }}</td>
                                <td>
                                    @if ($skill->status == 'active')
                                        <span class="label label-primary">Active</span>
                                    @else
                                        <span class="label label-danger">In-Active</span>
                                    @endif
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <a href="{{route('skills.show',$skill->id) }}" class="btn-white btn btn-xs" >View</a>

                                        <a href="{{route('skills.edit',$skill->id) }}" class="btn-white btn btn-xs">edit</a>

                                        <form action="{{ route('skills.destroy',$skill->id) }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn-white btn btn-xs">Delete</button>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
                </div>
            </div>
        </div>
        </div>
    </div>
@endsection

@section('script')
    {{-- Datatable --}}
    <script src=" {{ asset('assets/js/plugins/dataTables/datatables.min.js') }}"></script>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            $('.skills-table').DataTable();

            @if(session('success'))
                toastr.success('{{ session("success") }}', 'Success');
            @endif


            $('.delete-skill').click(function () {

                let experienceId = $(this).data('skill-id');

                // Generate Token method 1
                // let token = $("meta[name='csrf-token']").attr("content");

                // Generate Token method 2
                let token = '{{ csrf_token() }}';

                let url = '{{ route("skills.destroy", ":id") }}';
                url = url.replace(':id', skillId);

                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this data!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                }, function () {

                    $.ajax(
                    {
                        url: url,
                        type: 'POST',
                        data: {
                            "_token": token,
                            "_method": 'DELETE',

                        },
                        success: function (){
                            swal("Deleted!", "skill has been deleted.", "success");
                            setTimeout(() => {
                                location.reload();
                            }, 3000);
                        }
                    });

                });
            });

        });
    </script>
@endsection
