<?php

namespace Database\Factories;

use App\Models\demo;
use Illuminate\Database\Eloquent\Factories\Factory;

class DemoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = demo::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
